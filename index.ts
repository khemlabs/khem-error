import { format } from "date-fns";
import type { NextFunction, Request, Response } from "express";
import { nanoid } from "nanoid";

/**
 * Enum of HTTP status codes.
 */
export enum HttpErrorCode {
  /**
   * The request was invalid or cannot be processed.
   */
  BAD_REQUEST = 400,
  /**
   * Authentication is required to access the requested resource.
   */
  UNAUTHORIZED = 401,
  /**
   * Payment is required to access the requested resource.
   */
  PAYMENT_REQUIRED = 402,
  /**
   * The server refuses to fulfill the request due to a client error.
   */
  FORBIDDEN = 403,
  /**
   * The requested resource could not be found.
   */
  NOT_FOUND = 404,
  /**
   * The method is not supported by the requested resource.
   */
  METHOD_NOT_ALLOWED = 405,
  /**
   * The server will not process the request because it does not support the major type of the content as specified in the request's Accept- header.
   */
  NOT_ACCEPTABLE = 406,
  /**
   * Proxy authentication required.
   */
  PROXY_AUTHENTICATION_REQUIRED = 407,
  /**
   * The request timed out or could not be processed due to a server failure.
   */
  REQUEST_TIMEOUT = 408,
  /**
   * Conflicting updates from multiple sources.
   */
  CONFLICT = 409,
  /**
   * The requested resource is no longer available.
   */
  GONE = 410,
  /**
   * The request could not be processed because the length of the data in the request exceeds the maximum allowed size.
   */
  LENGTH_REQUIRED = 411,
  /**
   * The server will not process the request because it has already been processed for that resource by another user.
   */
  PRECONDITION_FAILED = 412,
  /**
   * The requested resource is too large or the length of the data exceeds the maximum allowed size.
   */
  REQUEST_ENTITY_TOO_LARGE = 413,
  /**
   * The requested URI is longer than the server will allow.
   */
  REQUEST_URI_TOO_LONG = 414,
  /**
   * The requested content type is not supported by this server.
   */
  UNSUPPORTED_MEDIA_TYPE = 415,
  /**
   * The requested range is not satisfiable.
   */
  RANGE_NOT_SATISFIABLE = 416,
  /**
   * Expectations failed, and the entity has no representation.
   */
  EXPECTATION_FAILED = 417,
  /**
   * The client must not repeat this request without modification.
   */
  PRECONDITION_REQUIRED = 428,
  /**
   * Too many requests have been made within a certain time frame.
   */
  TOO_MANY_REQUESTS = 429,
  /**
   * The requested resource is no longer available due to legal reasons.
   */
  UNAVAILABLE_FOR_LEGAL_REASONS = 451,
}

/**
 * Interface for errors with HTTP status codes.
 */
type Dictionary = {
  [key in string]: string | number | Dictionary;
};

/**
 * Class representing an HTTP error.
 *
 * To use this class in an Express.js handler:
 *
 * 1. Create a new instance of `HttpError` by passing in the desired HTTP status code and optional message data.
 * 2. Throw the error from your route handler to trigger the error handling logic, which will be handled by your global error handler (`ErrorHandler`).
 *
 * Example usage:
 * ```javascript
 * app.get('/example', (req, res) => {
 *     // Your code ...
 *     // This will be catch by the Smaug global error handler
 *     throw new HttpError(HttpErrorCode.BAD_REQUEST, 'Invalid request');
 * });
 * ```
 */
export class HttpError extends Error {
  public id: string;

  public type: string;

  public error?: any;

  /**
   * The HTTP status code of the error.
   */
  public status: HttpErrorCode;

  /**
   * Additional data for the error.
   */
  public data?: Dictionary;

  /**
   * Constructor for an HTTP error.
   *
   * @param {HttpErrorCode} status - The HTTP status code of the error.
   * @param {string} message - A human-readable description of the error.
   * @param {Dictionary} [data] - Additional data for the error (optional).
   * @param {any} error - An error that must be logged
   */
  constructor(
    status: HttpErrorCode,
    message: string,
    data?: Dictionary,
    error?: any
  ) {
    super(message);
    this.type = "HttpError";
    this.status = status;
    this.data = data;
    this.id = nanoid();

    if (error) {
      logError(error, this.id, "http");
    }
  }
}

/**
 * Class representing an unexpected error.
 *
 * To use this class in an Express.js handler:
 *
 * 1. Create a new instance of `UnexpectedError` by passing in the desired HTTP status code and optional message data.
 * 2. Throw the error from your route handler to trigger the error handling logic, which will be handled by your global error handler (`ErrorHandler`).
 *
 * Example usage:
 * ```javascript
 * app.get('/example', (req, res) => {
 *     // Your code ...
 *     // This will be catch by the Smaug global error handler
 *     throw new UnexpectedError('Invalid request');
 * });
 * ```
 */
export class UnexpectedError extends Error {
  public id: string;

  public type: string;

  public error?: any;

  /**
   * Additional data for the error (optional).
   */
  public data?: Dictionary;

  /**
   * Constructor for an unexpected error.
   *
   * @param {string} message - A human-readable description of the error.
   * @param {Dictionary} [data] - Additional data for the error (optional).
   * @param {any} error - An error that must be logged
   */
  constructor(message: string, data?: Dictionary, error?: any) {
    super(message);
    this.type = "UnexpectedError";
    this.data = data;
    this.error = error;
    this.id = nanoid();
    if (error) {
      logError(error, this.id, "unexpected");
    }
  }
}

/**
 * Error handler function.
 *
 * @param {any} err - The error to handle.
 * @param {Request} _req - The request that triggered the error.
 * @param {Response} res - The response object.
 * @param {NextFunction} _next - The next middleware in the chain.
 */
export function ErrorHandler(
  err: any,
  _req: Request,
  res: Response,
  _next: NextFunction
) {
  try {
    throw err;
  } catch (error) {
    const isSmaugError = isHttpError(error) || isUnexpectedError(error);
    const errorId = isSmaugError ? error.id : nanoid();
    const message = isSmaugError
      ? error.message
      : "An unexpected error occurred.";
    const data = isSmaugError ? error.data : {};

    // Smaug errors only logs if an error was received
    // That's why we need to log errors of other types or if its a Smaug errors that didn't receive one:
    if (!isSmaugError || !error.error) {
      logError(error, errorId, isHttpError(error) ? "http" : "unexpected");
    }

    return res
      .status(isHttpError(error) ? error.status : 500)
      .send({ errorId, message, data });
  }
}

function isHttpError(error: any): error is HttpError {
  return !!error && error.type === "HttpError";
}

function isUnexpectedError(error: any): error is UnexpectedError {
  return !!error && error.type === "UnexpectedError";
}

function logError(error: any, errorId: string, type: "unexpected" | "http") {
  console.error(
    `\x1b[30m[Smaug]\x1b[0m ${type === "http" ? "\x1b[33m" : "\x1b[31m"} ${
      type === "http" ? "HTTP" : "Unexpected"
    } Error\x1b[0m (id: \x1b[30m${errorId}\x1b[0m, date: ${format(
      new Date(),
      "yyy-MM-dd hh:mm:ss.sss"
    )}): `,
    error
  );
}
