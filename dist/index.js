"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ErrorHandler = exports.UnexpectedError = exports.HttpError = exports.HttpErrorCode = void 0;
var date_fns_1 = require("date-fns");
var nanoid_1 = require("nanoid");
/**
 * Enum of HTTP status codes.
 */
var HttpErrorCode;
(function (HttpErrorCode) {
    /**
     * The request was invalid or cannot be processed.
     */
    HttpErrorCode[HttpErrorCode["BAD_REQUEST"] = 400] = "BAD_REQUEST";
    /**
     * Authentication is required to access the requested resource.
     */
    HttpErrorCode[HttpErrorCode["UNAUTHORIZED"] = 401] = "UNAUTHORIZED";
    /**
     * Payment is required to access the requested resource.
     */
    HttpErrorCode[HttpErrorCode["PAYMENT_REQUIRED"] = 402] = "PAYMENT_REQUIRED";
    /**
     * The server refuses to fulfill the request due to a client error.
     */
    HttpErrorCode[HttpErrorCode["FORBIDDEN"] = 403] = "FORBIDDEN";
    /**
     * The requested resource could not be found.
     */
    HttpErrorCode[HttpErrorCode["NOT_FOUND"] = 404] = "NOT_FOUND";
    /**
     * The method is not supported by the requested resource.
     */
    HttpErrorCode[HttpErrorCode["METHOD_NOT_ALLOWED"] = 405] = "METHOD_NOT_ALLOWED";
    /**
     * The server will not process the request because it does not support the major type of the content as specified in the request's Accept- header.
     */
    HttpErrorCode[HttpErrorCode["NOT_ACCEPTABLE"] = 406] = "NOT_ACCEPTABLE";
    /**
     * Proxy authentication required.
     */
    HttpErrorCode[HttpErrorCode["PROXY_AUTHENTICATION_REQUIRED"] = 407] = "PROXY_AUTHENTICATION_REQUIRED";
    /**
     * The request timed out or could not be processed due to a server failure.
     */
    HttpErrorCode[HttpErrorCode["REQUEST_TIMEOUT"] = 408] = "REQUEST_TIMEOUT";
    /**
     * Conflicting updates from multiple sources.
     */
    HttpErrorCode[HttpErrorCode["CONFLICT"] = 409] = "CONFLICT";
    /**
     * The requested resource is no longer available.
     */
    HttpErrorCode[HttpErrorCode["GONE"] = 410] = "GONE";
    /**
     * The request could not be processed because the length of the data in the request exceeds the maximum allowed size.
     */
    HttpErrorCode[HttpErrorCode["LENGTH_REQUIRED"] = 411] = "LENGTH_REQUIRED";
    /**
     * The server will not process the request because it has already been processed for that resource by another user.
     */
    HttpErrorCode[HttpErrorCode["PRECONDITION_FAILED"] = 412] = "PRECONDITION_FAILED";
    /**
     * The requested resource is too large or the length of the data exceeds the maximum allowed size.
     */
    HttpErrorCode[HttpErrorCode["REQUEST_ENTITY_TOO_LARGE"] = 413] = "REQUEST_ENTITY_TOO_LARGE";
    /**
     * The requested URI is longer than the server will allow.
     */
    HttpErrorCode[HttpErrorCode["REQUEST_URI_TOO_LONG"] = 414] = "REQUEST_URI_TOO_LONG";
    /**
     * The requested content type is not supported by this server.
     */
    HttpErrorCode[HttpErrorCode["UNSUPPORTED_MEDIA_TYPE"] = 415] = "UNSUPPORTED_MEDIA_TYPE";
    /**
     * The requested range is not satisfiable.
     */
    HttpErrorCode[HttpErrorCode["RANGE_NOT_SATISFIABLE"] = 416] = "RANGE_NOT_SATISFIABLE";
    /**
     * Expectations failed, and the entity has no representation.
     */
    HttpErrorCode[HttpErrorCode["EXPECTATION_FAILED"] = 417] = "EXPECTATION_FAILED";
    /**
     * The client must not repeat this request without modification.
     */
    HttpErrorCode[HttpErrorCode["PRECONDITION_REQUIRED"] = 428] = "PRECONDITION_REQUIRED";
    /**
     * Too many requests have been made within a certain time frame.
     */
    HttpErrorCode[HttpErrorCode["TOO_MANY_REQUESTS"] = 429] = "TOO_MANY_REQUESTS";
    /**
     * The requested resource is no longer available due to legal reasons.
     */
    HttpErrorCode[HttpErrorCode["UNAVAILABLE_FOR_LEGAL_REASONS"] = 451] = "UNAVAILABLE_FOR_LEGAL_REASONS";
})(HttpErrorCode = exports.HttpErrorCode || (exports.HttpErrorCode = {}));
/**
 * Class representing an HTTP error.
 *
 * To use this class in an Express.js handler:
 *
 * 1. Create a new instance of `HttpError` by passing in the desired HTTP status code and optional message data.
 * 2. Throw the error from your route handler to trigger the error handling logic, which will be handled by your global error handler (`ErrorHandler`).
 *
 * Example usage:
 * ```javascript
 * app.get('/example', (req, res) => {
 *     // Your code ...
 *     // This will be catch by the Smaug global error handler
 *     throw new HttpError(HttpErrorCode.BAD_REQUEST, 'Invalid request');
 * });
 * ```
 */
var HttpError = /** @class */ (function (_super) {
    __extends(HttpError, _super);
    /**
     * Constructor for an HTTP error.
     *
     * @param {HttpErrorCode} status - The HTTP status code of the error.
     * @param {string} message - A human-readable description of the error.
     * @param {Dictionary} [data] - Additional data for the error (optional).
     * @param {any} error - An error that must be logged
     */
    function HttpError(status, message, data, error) {
        var _this = _super.call(this, message) || this;
        _this.type = "HttpError";
        _this.status = status;
        _this.data = data;
        _this.id = (0, nanoid_1.nanoid)();
        if (error) {
            logError(error, _this.id, "http");
        }
        return _this;
    }
    return HttpError;
}(Error));
exports.HttpError = HttpError;
/**
 * Class representing an unexpected error.
 *
 * To use this class in an Express.js handler:
 *
 * 1. Create a new instance of `UnexpectedError` by passing in the desired HTTP status code and optional message data.
 * 2. Throw the error from your route handler to trigger the error handling logic, which will be handled by your global error handler (`ErrorHandler`).
 *
 * Example usage:
 * ```javascript
 * app.get('/example', (req, res) => {
 *     // Your code ...
 *     // This will be catch by the Smaug global error handler
 *     throw new UnexpectedError('Invalid request');
 * });
 * ```
 */
var UnexpectedError = /** @class */ (function (_super) {
    __extends(UnexpectedError, _super);
    /**
     * Constructor for an unexpected error.
     *
     * @param {string} message - A human-readable description of the error.
     * @param {Dictionary} [data] - Additional data for the error (optional).
     * @param {any} error - An error that must be logged
     */
    function UnexpectedError(message, data, error) {
        var _this = _super.call(this, message) || this;
        _this.type = "UnexpectedError";
        _this.data = data;
        _this.error = error;
        _this.id = (0, nanoid_1.nanoid)();
        if (error) {
            logError(error, _this.id, "unexpected");
        }
        return _this;
    }
    return UnexpectedError;
}(Error));
exports.UnexpectedError = UnexpectedError;
/**
 * Error handler function.
 *
 * @param {any} err - The error to handle.
 * @param {Request} _req - The request that triggered the error.
 * @param {Response} res - The response object.
 * @param {NextFunction} _next - The next middleware in the chain.
 */
function ErrorHandler(err, _req, res, _next) {
    try {
        throw err;
    }
    catch (error) {
        var isSmaugError = isHttpError(error) || isUnexpectedError(error);
        var errorId = isSmaugError ? error.id : (0, nanoid_1.nanoid)();
        var message = isSmaugError
            ? error.message
            : "An unexpected error occurred.";
        var data = isSmaugError ? error.data : {};
        // Smaug errors only logs if an error was received
        // That's why we need to log errors of other types or if its a Smaug errors that didn't receive one:
        if (!isSmaugError || !error.error) {
            logError(error, errorId, isHttpError(error) ? "http" : "unexpected");
        }
        return res
            .status(isHttpError(error) ? error.status : 500)
            .send({ errorId: errorId, message: message, data: data });
    }
}
exports.ErrorHandler = ErrorHandler;
function isHttpError(error) {
    return !!error && error.type === "HttpError";
}
function isUnexpectedError(error) {
    return !!error && error.type === "UnexpectedError";
}
function logError(error, errorId, type) {
    console.error("\u001B[30m[Smaug]\u001B[0m ".concat(type === "http" ? "\x1b[33m" : "\x1b[31m", " ").concat(type === "http" ? "HTTP" : "Unexpected", " Error\u001B[0m (id: \u001B[30m").concat(errorId, "\u001B[0m, date: ").concat((0, date_fns_1.format)(new Date(), "yyy-MM-dd hh:mm:ss.sss"), "): "), error);
}
//# sourceMappingURL=index.js.map